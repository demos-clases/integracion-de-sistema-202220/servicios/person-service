require('dotenv').config()

module.exports = {
  port: process.env.PORT,
  personDb: process.env.DB_PERSON_CONECTION_STRING,
  jwtSecret: process.env.JWT_SECRET,
  services: {
    auth: process.env.AUTH_SERVICE_URL,
  },
  token: {
    publicKey: process.env.RSA_PUBLIC_KEY_FILE,
  },
}