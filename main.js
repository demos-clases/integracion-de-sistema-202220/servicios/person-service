const express = require('express');
const config = require('./config');
const authMdw = require('./app/middlewares/jwt.middleware');
const mongoose = require('mongoose');
const personRoutes = require('./app/routes/person.routes');

mongoose.connect(config.personDb);

const app = new express();
app.use(express.json());

app.get('/', function(req, res) {
  const data = {
    status: 'success',
    data: {
      message: 'Hello World'
    }
  };
  res.json(data);
});

app.use('/api/v1/person', personRoutes);

app.listen(config.port, function() {
  console.info('Servidor en express iniciado en el puerto 8000');
});