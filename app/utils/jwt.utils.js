const jwt = require('jsonwebtoken');
const fs = require('fs');
const config = require('./../../config');

exports.validateToken = (token) => {
  const publicKey = fs.readFileSync(config.token.publicKey).toString();
  console.log('pub', publicKey);
  const properties = {algorithms: ['RS256']};
  const decoded = jwt.verify(token, publicKey, properties);
  return decoded;
};
