const express = require('express');
const PersonaCtrl = require('./../controllers/person.controller');

const router = express.Router();


router.post('/', PersonaCtrl.create);
router.get('/:id', PersonaCtrl.get);
router.put('/:id', PersonaCtrl.update);
router.delete('/:id', PersonaCtrl.remove);


module.exports = router;