const config = require('./../../config');
const jwt = require('./../utils/jwt.utils');

exports.hasValidToken = async (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    res.status(403).json({error: 'Token invalido'});
    return;
  } 
  // bearer token
  token = token.replace('Bearer ', '');
  try {
    // valida el token con la llave publica
    let decoded = jwt.validateToken(token);
    next();
    /*
    // validar la firma del token
    const response = await axios.post(
      `${config.services.auth}/auth/verify`, 
      {token});
    
    console.log(response);
    next();
   */
  } catch (err) {
    res.status(403).json({error: 'Token invalido'});
  }
  
}