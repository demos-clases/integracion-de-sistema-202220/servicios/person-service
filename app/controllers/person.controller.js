const PersonModel = require('./../models/person.model');

exports.create = async function(req, res) {
  const personData = req.body;
  try {
    const person = await PersonModel.create(personData);
    res.status(201).json(person);
  } catch(err) {
    res.status(500).json({error: err.message});
  }
}

exports.get =  async function(req, res) {
  const id = req.params.id;
  // llamar a la base de datos y traer el usuario con el id
  try {
    const person = await PersonModel.findById(id)
      .select('names lastNames email -_id')
      .exec();
    !person
      ? res.status(404).json({error: 'Persona no encontrada'})
      : res.status(200).json(person);
  } catch (err) {
    res.status(500).json({error: 'error interno'});
  }
}

exports.update = async function(req, res) {
  const id = req.params.id;
 
  try {
    await CourseModel.findByIdAndUpdate(id, req.body).exec();
    res.json({ok: 'ok'});
  } catch (err) {
    res.status(500).json({error: 'error interno'});
  }
}

exports.remove = async function(req, res) {
  const id = req.params.id;

  try {
    await PersonModel.findByIdAndRemove(id).exec();
    res.status(204).json({ status: 'success'});
  } catch (err) {
    res.status(500).json({error: 'error interno'});
  }
}