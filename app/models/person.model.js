const mongoose = require('mongoose');
const { Schema } = mongoose;

const PersonSchema = new Schema({
  names: { type: String, required: true },
  lastNames: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  birthdate: { type: Date, required: false },
  gender: { type: String, required: false }
}, 
{ timestamps: true }
);

// Compile model from schema
var PersonModel = mongoose.model('Person', PersonSchema );

module.exports = PersonModel;
